#/bin/bash!

rsync -avzP --delete --exclude=node_modules --exclude=.git --exclude=.DS_Store /Users/pauriquelme/Keteden/C2C/website/. c2csti@185.127.128.163:/home/c2csti/www/c2c-sti.com/server
ssh -t c2csti@185.127.128.163 'bash -i -c "
cd /home/c2csti/www/c2c-sti.com/server;
npm install;

cd /home/c2csti/www/c2c-sti.com/server/air_modules/kDatabase;
npm install;

cd /home/c2csti/www/c2c-sti.com/server/air_modules/kTemplate;
npm install;

cd /home/c2csti/www/c2c-sti.com/server/src;
pm2 restart index.js;
"'
