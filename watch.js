const chokidar = require("chokidar");
const { exec } = require("child_process");

const watcher = chokidar.watch("./website");

console.log("Running...");
watcher.on("add", path => {
  console.log("Something new!", path);
  run();
});

watcher.on("change", path => {
  console.log("Change detected!", path);
  run();
});

watcher.on("unlink", path => {
  console.log("Something deleted!", path);
  run();
});

watcher.on("error", err => {
  console.log("Error!", err);
});

const run = () => {
  debounce(() => {
    console.log("Compiling...");
    exec("node compile.js", (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
      }
    });
  }, 300);
};

let isFinished = true;
const debounce = (action, debounceTime) => {
  if (isFinished) {
    isFinished = false;
    setTimeout(() => {
      action();
      isFinished = true;
    }, debounceTime);
  }
};
