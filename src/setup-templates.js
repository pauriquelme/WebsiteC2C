const { kTemplate } = require("../air_modules/kTemplate/index");

const { home } = require("./views/home/home");
const { home_hero } = require("./views/home/hero/hero");
const { home_guarantees } = require("./views/home/guarantees/guarantees");
const { home_accrd } = require("./views/home/accrd/accrd");
const { home_services } = require("./views/home/services/services");
const { home_works } = require("./views/home/works/works");
const { home_facilities } = require("./views/home/facilities/facilities");
const { home_blog } = require("./views/home/blog/blog");
const { home_contact } = require("./views/home/contact/contact");
const { footer } = require("./views/footer/footer");
const { blog } = require("./views/blog/blog");
const { post_card } = require("./views/blog/post-card/post-card");
const { post } = require("./views/blog/post/post");
const { basic_hero } = require("./components/basic-hero/basic-hero");
const { navbar } = require("./components/navbar/navbar");
const { services } = require("./views/services/services");
const { data_point } = require("./components/data-point/data-point");
const { about } = require("./views/about/about");

// -----------------------------------
// SETUP
// -----------------------------------

// Setup kTemplate
const compile = kTemplate({
  components: {
    home,
    home_hero,
    home_guarantees,
    home_accrd,
    home_services,
    home_works,
    home_facilities,
    home_blog,
    home_contact,
    footer,
    blog,
    post_card,
    post,
    basic_hero,
    navbar,
    services,
    data_point,
    about,
  },
  conf: {
    lang: "es",
    head: [
      '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5"/>',
      '<meta name="description" content="Sitio web de C2C Servicios Técnicos de Inspección">',
      '<meta name="ahrefs-site-verification" content="f6284fdc7e1dcbe346e2a6a533cd36ea9c7855fa3ed25604fa9832e50a902a58"></meta>',
      '<meta name="theme-color" content="#232323" />',
      '<script src="/assets/navbar.js"></script>',
      //Pixel Code for https://overtracking.com/
      //'<script defer src="https://overtracking.com/p/lSg1JU5qlccWldFh"></script>',
      '<link rel="preconnect" href="https://fonts.googleapis.com">',
      '<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>',
      '<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;600&family=Raleway:wght@700;900&display=swap">',
      '<link rel="stylesheet" async defer href="/assets/style.css">',
      '<link rel="stylesheet" async defer href="/assets/reset.css">',
      '<link rel="stylesheet" async defer href="/assets/theme.css">',
      '<link rel="stylesheet" async defer href="/assets/fontawesome/css/all.min.css">',
    ],
    favicon: "/assets/favicon.ico",
    assetsFolder: "/assets",
  },
});

module.exports = {
  compile,
};
