const { db } = require("../../setup-database");
const { file } = require("../../utils/utils");

const about = {
  template: file(__dirname + "/about.html"),
  style: file(__dirname + "/about.css"),
  compile() {
    const about = db.get("es.about");

    return Promise.resolve({
      ...about,
    });
  },
};

module.exports = {
  about,
};
