const express = require("express");
const { db } = require("../../setup-database");
const { compile } = require("../../setup-templates");
const { about } = require("./about");

const aboutRoutes = express();

// BLOG POST
aboutRoutes.get("/nosotros", (req, res) => {
  compile({
    title: "C2C-STI · Nosotros",
    component: about,
  }).then(compiledPage => {
    res.send(compiledPage);
  });
});

module.exports = {
  aboutRoutes,
};
