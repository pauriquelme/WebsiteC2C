const { db } = require("../../setup-database");
const { file, objToQueryParams, uniqueValues } = require("../../utils/utils");

const blog = {
  tag: "blog",
  template: file(__dirname + "/blog.html"),
  style: file(__dirname + "/blog.css"),
  compile({ query }) {
    const posts = db.get("post.content");
    const { pager, chips, currentPosts } = preparePageLinks(posts, query);

    return Promise.resolve({
      ...db.get("es.blog_gallery"),
      chips,
      posts: currentPosts,
      pager,
    });
  },
};

const pageSize = 15;
function preparePageLinks(posts, query) {
  const { page = 1, activeChips, search } = query;
  const splittedChips = (activeChips || "").split(";").filter(it => it !== "");

  posts = posts
    .filter(post => {
      // FILTER BY CHIPS
      if (splittedChips.length === 0) return true;
      const postTags = post.tags.split(";");

      let i = 0;
      while (i < splittedChips.length) {
        if (postTags.includes(splittedChips[i])) {
          i++;
          continue;
        } else {
          return false;
        }
      }

      return true;
    })
    .filter(post => {
      // FILTER BY TEXT
      if (!search || search === "") return true;
      const _search = search.trim().toLowerCase();

      return (
        post.content.trim().toLowerCase().includes(_search) ||
        post.category.trim().toLowerCase().includes(_search) ||
        post.title.trim().toLowerCase().includes(_search) ||
        post.description.trim().toLowerCase().includes(_search) ||
        post.author.name.trim().toLowerCase().includes(_search) ||
        post.tags.trim().toLowerCase().includes(_search)
      );
    });

  const currentPosts = posts.filter((_, i) => isInPage(i, page, pageSize));

  // SETUP CHIPS
  const tags = posts
    .reduce((a, b) => a + b.tags + ";", "")
    .split(";")
    .filter(chip => chip !== "")
    .filter(uniqueValues);

  const chips = tags.map(tag => {
    let active = splittedChips.includes(tag);

    const currentChips = (
      active
        ? splittedChips.filter(chip => chip !== tag)
        : [...splittedChips, tag]
    )
      .filter(chip => chip !== "")
      .join(";");

    return {
      active,
      name: tag,
      link: objToQueryParams({
        page: 1,
        activeChips: currentChips === "" ? undefined : currentChips,
        search,
      }),
    };
  });

  // SETUP PAGER
  const pager = [];
  const totalPageNumber = Math.ceil(posts.length / pageSize);

  let i = 0;
  while (i < totalPageNumber) {
    pager.push({
      number: i + 1,
      active: parseInt(page) === i + 1,
      link: objToQueryParams({ page: i + 1, activeChips, search }),
    });
    i++;
  }

  return {
    pager,
    chips,
    currentPosts,
  };
}

const isInPage = (i, page, pageSize) => {
  const maxValue = page * pageSize;
  const minValue = (page - 1) * pageSize;
  return i >= minValue && i < maxValue;
};

module.exports = {
  blog,
};
