const { file } = require("../../../utils/utils");

const post_card = {
  tag: "post-card",
  template: file(__dirname + "/post-card.html"),
  style: file(__dirname + "/post-card.css"),
  compile({ category, chips, size, theme, image, opacity = "0.7" }) {
    let color, bgColor, accent;

    if (theme === "orange") {
      color = "c-primary";
      bgColor = "bg-accent";
      accent = "c-secondary";
    } else if (theme === "blue") {
      color = "c-secondary";
      bgColor = "bg-primary";
      accent = "c-accent";
    } else if (theme === "white") {
      color = "c-primary";
      bgColor = "bg-secondary";
      accent = "c-accent";
    } else if (theme === "black" || theme === "gray") {
      color = "c-white";
      bgColor = "bg-black";
      accent = "c-accent";
    }

    return {
      opacity,
      image,
      color,
      size,
      bgColor,
      accent,
      category,
      chips: (chips || "").split(";"),
    };
  },
};

module.exports = {
  post_card,
};
