const { file } = require("../../../utils/utils");
const { db } = require("../../../setup-database");

const post = {
  tag: "post",
  template: file(__dirname + "/post.html"),
  style: file(__dirname + "/post.css"),
  compile({ postData }) {
    let posts = db.get("post.content");

    posts = posts.filter(post => post.url !== postData.url);

    // 1 Same Tags
    let sameTags;
    let maxCoincidentTags = 0;
    const currentPostTags = postData.tags.split(";");

    for (let i = 0; i < posts.length; i++) {
      const post = posts[i];
      const postTags = post.tags.split(";").map(it => it.toLowerCase().trim());
      let coincidentTags = 0;

      currentPostTags.forEach(tag => {
        if (postTags.includes(tag.toLowerCase().trim())) {
          coincidentTags++;
        }
      });

      if (coincidentTags > maxCoincidentTags) {
        maxCoincidentTags = coincidentTags;
        sameTags = post;
      }
    }

    if (!sameTags) sameTags = posts[Math.floor(Math.random() * posts.length)];

    posts = posts.filter(post => post.url !== sameTags.url);

    // 2 Same Category
    const sameCategory =
      posts.find(
        post => post.category.toLowerCase() === postData.category.toLowerCase()
      ) || posts[Math.floor(Math.random() * posts.length)];

    posts = posts.filter(post => post.url !== sameCategory.url);

    // 3 Same Author
    const sameAuthor =
      posts.find(
        post =>
          post.author.name.toLowerCase() === postData.author.name.toLowerCase()
      ) || posts[Math.floor(Math.random() * posts.length)];

    return Promise.resolve({
      ...postData,
      posts: [sameTags, sameCategory, sameAuthor],
    });
  },
};

module.exports = {
  post,
};
