const express = require("express");
const { blog } = require("./blog");
const { post } = require("./post/post");
const { compile } = require("../../setup-templates");
const { db } = require("../../setup-database");
const blogRoutes = express();

// BLOG GALLERY
blogRoutes.get("/blog", (req, res) => {
  compile(
    {
      title: "C2C-STI / Blog",
      component: blog,
      head: ['<script src="/assets/searcher.js"></script>'],
    },
    { query: req.query }
  ).then(compiledPage => {
    res.send(compiledPage);
  });
});

// BLOG POST
blogRoutes.get("/blog/:postName", (req, res) => {
  const postData = db
    .get("post.content")
    .find(post => post.url === "/blog/" + req.params.postName);

  if (!postData) {
    res.sendStatus(404);
  } else
    compile(
      {
        title: "C2C · " + postData.title,
        component: post,
      },
      { postData }
    ).then(compiledPage => {
      res.send(compiledPage);
    });
});

module.exports = {
  blogRoutes,
};
