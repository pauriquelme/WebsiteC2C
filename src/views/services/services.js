const { file } = require("../../utils/utils");

const services = {
  template: file(__dirname + "/services.html"),
  style: file(__dirname + "/services.css"),
  compile({ service, _id }) {
    service.links = service.links.filter(link => link.url !== _id);
    return Promise.resolve({
      ...service,
    });
  },
};

module.exports = {
  services,
};
