const express = require("express");
const { db } = require("../../setup-database");
const { compile } = require("../../setup-templates");
const { services } = require("./services");

const servicesRoutes = express();

// BLOG POST
servicesRoutes.get("/servicio/:service_id", (req, res) => {
  const _id = "/servicio/" + req.params.service_id;

  const service = (db.get("es.services.content") || []).find(
    service => service._id === _id
  );

  compile(
    {
      title: "C2C-STI · " + service.hero.title,
      component: services,
    },
    { service, _id }
  ).then(compiledPage => {
    res.send(compiledPage);
  });
});

module.exports = {
  servicesRoutes,
};
