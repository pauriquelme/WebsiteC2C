const { db } = require("../../setup-database");
const { file } = require("../../utils/utils");

const footer = {
  tag: "footer",
  template: file(__dirname + "/footer.html"),
  style: file(__dirname + "/footer.css"),
  compile() {
    const year = new Date().getFullYear();
    const copyRightSimbol = "©";
    return Promise.resolve({
      year,
      copyRightSimbol,
      ...db.get("es.footer"),
    });
  },
};

module.exports = {
  footer,
};
