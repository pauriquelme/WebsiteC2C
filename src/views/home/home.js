const { db } = require("../../setup-database");
const { file } = require("../../utils/utils");

const home = {
  tag: "home",
  template: file(__dirname + "/home.html"),
  style: file(__dirname + "/home.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home"),
    });
  },
};

module.exports = {
  home,
};
