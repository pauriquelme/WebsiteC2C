const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_blog = {
  tag: "blog",
  template: file(__dirname + "/blog.html"),
  style: file(__dirname + "/blog.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home.blog"),
      posts: db.get("post.content").slice(0, 4),
    });
  },
};

module.exports = {
  home_blog,
};
