const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_guarantees = {
  tag: "guarantees",
  template: file(__dirname + "/guarantees.html"),
  style: file(__dirname + "/guarantees.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home.guarantees"),
      /*title: "Nuestras garantías",
      subtitle: "Nuestro laboratorio de control de calidad te ofrece",*/
    });
  },
};

module.exports = {
  home_guarantees,
};
