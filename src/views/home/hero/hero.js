const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_hero = {
  tag: "hero",
  template: file(__dirname + "/hero.html"),
  style: file(__dirname + "/hero.css"),
  compile() {
    return {
      ...db.get("es.home.hero"),
    };
  },
};

module.exports = {
  home_hero,
};
