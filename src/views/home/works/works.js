const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_works = {
  tag: "projects",
  template: file(__dirname + "/works.html"),
  style: file(__dirname + "/works.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home.works"),
      /*{
          title: "Cómo vivir del aire",
          image: "./assets/img/edificio-1.jpeg",
          description:
            "Fantasía de ficción adaptada de la biografía del rey J.C. primero",
        },
        {
          title: "Más leña que hace frio",
          image: "./assets/img/edificio-3.jpg",
          description:
            "De los creadores de winter is coming se viene más leña que hace frio.",
        },
        {
          title: "Autobuses, ¿Es un auto? ¿Es un bus?",
          image: "./assets/img/fabrica.jpg",
          description:
            "Es un autobus, los expertos afirman que sirve para transportar personas del puto A al punto B. El misterio de su caída en desuso todavía no se ha resuelto",
        },*/
    });
  },
};

module.exports = {
  home_works,
};
