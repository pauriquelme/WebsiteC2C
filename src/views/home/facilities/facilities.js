const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_facilities = {
  tag: "facilities",
  template: file(__dirname + "/facilities.html"),
  style: file(__dirname + "/facilities.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home.facilities"),
    });
  },
};

module.exports = {
  home_facilities,
};
