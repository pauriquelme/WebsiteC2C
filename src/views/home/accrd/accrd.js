const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_accrd = {
  tag: "accreditation",
  template: file(__dirname + "/accrd.html"),
  style: file(__dirname + "/accrd.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home.accrd"),
    });
  },
};

module.exports = {
  home_accrd,
};
