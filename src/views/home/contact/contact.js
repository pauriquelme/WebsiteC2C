const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_contact = {
  tag: "contact",
  template: file(__dirname + "/contact.html"),
  style: file(__dirname + "/contact.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home.contact"),
    });
  },
};

module.exports = {
  home_contact,
};
