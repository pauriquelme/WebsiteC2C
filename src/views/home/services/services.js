const { db } = require("../../../setup-database");
const { file } = require("../../../utils/utils");

const home_services = {
  tag: "services",
  template: file(__dirname + "/services.html"),
  style: file(__dirname + "/services.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.home.services"),
    });
  },
};

module.exports = {
  home_services,
};
