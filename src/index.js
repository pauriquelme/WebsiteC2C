const express = require("express");
const { blogRoutes } = require("./views/blog/blog.routes");
const { homeRoutes } = require("./views/home/home-routes");
const { servicesRoutes } = require("./views/services/services.routes");
const { aboutRoutes } = require("./views/about/about.routes");

// -----------------------------------
// SETUP
// -----------------------------------
const app = express();

// Setup static assets folder
app.use("/assets", express.static("./assets"));

// Setup routes
app.use(homeRoutes);
app.use(blogRoutes);
app.use(servicesRoutes);
app.use(aboutRoutes);

// Run server
app.listen(3000, () => {
  console.log("SERVER LISTENING IN PORT 3000");
});
