const { file } = require("../../utils/utils");

const data_point = {
  tag: "data-point",
  template: file(__dirname + "/data-point.html"),
  style: file(__dirname + "/data-point.css"),
  compile({ icon, title, description, list }) {
    return {
      icon,
      title,
      description,
      list: list ? list.split(";") : false,
    };
  },
};

module.exports = {
  data_point,
};
