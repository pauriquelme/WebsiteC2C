const { file } = require("../../utils/utils");

const basic_hero = {
  template: file(__dirname + "/basic-hero.html"),
  style: file(__dirname + "/basic-hero.css"),
  compile({
    image_src,
    theme,
    height = "500px",
    opacity = "0.7",
    font_size = "1em",
  }) {
    let bg = "bg-primary",
      c = "c-secondary";

    if (theme === "blue") {
      bg = "bg-primary";
      c = "c-secondary";
    } else if (theme === "white") {
      bg = "bg-secondary";
      c = "c-primary";
    } else if (theme === "orange") {
      bg = "bg-accent";
      c = "c-primary";
    }

    return Promise.resolve({
      font_size,
      bg,
      c,
      image_src,
      height,
      opacity,
    });
  },
};

module.exports = {
  basic_hero,
};
