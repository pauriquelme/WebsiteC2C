const { db } = require("../../setup-database");
const { file } = require("../../utils/utils");

const navbar = {
  template: file(__dirname + "/navbar.html"),
  style: file(__dirname + "/navbar.css"),
  compile() {
    return Promise.resolve({
      ...db.get("es.navbar"),
    });
  },
};

module.exports = {
  navbar,
};
