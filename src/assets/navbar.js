window.addEventListener("load", () => {
  const navbarMenu = document.getElementById("mobile-menu");
  const openMenu = document.getElementById("mobile-menu-open");
  const closeMenu = document.getElementById("mobile-menu-close");
  const clickableLinks = document.querySelectorAll(".click-link");

  openMenu.addEventListener("click", () => {
    navbarMenu.style.top = "0%";
  });

  [closeMenu, ...clickableLinks].forEach(element => {
    element.addEventListener("click", () => {
      navbarMenu.style.top = "-100%";
    });
  });
});
