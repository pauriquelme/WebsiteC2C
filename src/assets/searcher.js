window.addEventListener("load", () => {
  const query = queryParamsToObj(location.search);
  const postInput = document.getElementById("search-posts");
  const searchLink = document.getElementById("search-link");

  postInput.addEventListener("keyup", e => {
    query.search = postInput.value;
    searchLink.href = objToQueryParams(query);
    if (e.keyCode === 13) {
      e.preventDefault();
      searchLink.click();
    }
  });
});

function queryParamsToObj(queryParams = "") {
  if (queryParams === "") return {};

  queryParams = queryParams.substring(1);

  const params = queryParams.split("&");
  const obj = {};
  params.forEach(param => {
    const [key, value] = param.split("=");
    if (value !== undefined) {
      obj[key] = value;
    }
  });

  return obj;
}

function objToQueryParams(obj) {
  if (!obj) return "";
  let queryString = "?";

  for (const key in obj) {
    if (obj[key] !== undefined) {
      if (queryString !== "?") queryString += "&";
      queryString += `${key}=${obj[key]}`;
    }
  }

  return queryString;
}
