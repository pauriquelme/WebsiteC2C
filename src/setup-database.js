const { kDatabase } = require("../air_modules/kDatabase/index");

// Setup Database
const db = kDatabase(__dirname + "/..", "c2c");

module.exports = {
  db,
};
